# Automação STORE
Projeto de automação de testes do store

## Clonar

Link para Clonar o Projeto:https://gitlab.com/djonathan_kaiper/divvino_store.git

## Uso

- Necessário tem o ruby instalado na maquina.
- Necessário tem o driver do firefox e Google Chrome na pasta bin do ruby: C:\Ruby26-x64\bin.
- Acessar a pasta do projeto clonado acima.
- Executar o comando: "bundle install"

Para executar algum cenário:
Exemplo:
         cucumber -t <TAG_CENARIO>
         cucumber -t @adicionar_produto

Para executar uma suíte do store:
Exemplo:
         cucumber -t <TAG_DA_SUITE>
         cucumber -t @store