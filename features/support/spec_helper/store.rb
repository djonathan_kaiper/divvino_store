Dir[File.join(File.dirname(__FILE__), 'page_objects/*.rb')].sort.each { |file| require file }

class StorePages
  def home
    Home.new
  end

  def login
    Login.new
  end

  def busca
    Busca.new
  end

  def carrinho
    Carrinho.new
  end
end