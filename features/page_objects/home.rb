class Home < SitePrism::Page
  set_url CONFIG['url']
  element :img_logo, '.logo'
  element :div_card_home, '.cart_circle'
end