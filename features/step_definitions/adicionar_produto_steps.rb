
Quando("adicionar produto no carrinho") do
  step 'realizar busca do produto "Cabernet"'
  step 'deve ser encontrado o produto "Cabernet"'
  @produto.has_div_adicionar?(wait: 10)
  @produto.div_adicionar.click
  $store_page.home.div_card_home.click
end

Então("deve ser apresentado produto no carrinho") do
  produto_carrinho = $store_page.carrinho.carrinho_produto.last
  expect(produto_carrinho.get_link).to eql(@produto_link)
  expect(produto_carrinho.a_name_carrinho.text).to have_content(@produto_name_busca)
  expect(produto_carrinho.div_valor_produto.text).to eql(@produto_preco_busca)
  expect(produto_carrinho.span_quantidade_produto.text).to eql(@produto_quantidade_busca)
end