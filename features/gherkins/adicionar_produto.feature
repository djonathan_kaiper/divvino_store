#language: pt

@store
Funcionalidade: Adicionar produto no carrinho
  Eu como usuário do sistema
  Quero poder adicionar um produto
  Para validar que produto adicionado é apresentado no carrinho

    @adicionar_produto
    Cenario: Validar adição de produto no carrinho
      Dado acessar o site da divvino
      Quando adicionar produto no carrinho
      Então deve ser apresentado produto no carrinho