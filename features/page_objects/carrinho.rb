class ProdutoCarrinho < SitePrism::Section
  element :a_name_carrinho, '.capitalize-text a'
  element :div_valor_produto, '.s25.pd_t_12'
  element :span_quantidade_produto, '.select2-selection__rendered'

  def get_link
    a_name_carrinho[:href]
  end
end

class Carrinho < SitePrism::Page
  sections :carrinho_produto, ProdutoCarrinho, '.division '
end