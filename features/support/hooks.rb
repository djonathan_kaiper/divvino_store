Dir[File.join(File.dirname(__FILE__), 'spec_helper/*.rb')].sort.each { |file| require file }

$store_page = StorePages.new

Before do
  page.current_window.resize_to(1920, 1080)
end

After do |scenario|
  screenshot = page.save_screenshot("report/screenshots/#{scenario.__id__}.png")
  embed(screenshot, 'image/png', 'Screenshot')
  Capybara.current_session.driver.quit
end