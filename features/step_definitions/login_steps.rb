Dado("acessar o site da divvino") do
  $store_page.home.load
  $store_page.home.wait_until_img_logo_visible(wait: 60)
end

Quando("realizar login com {string} e {string}") do |email, senha|
  $store_page.login.wait_until_a_entrar_login_visible(wait: 60)
  $store_page.login.a_entrar_login.click
  $store_page.login.inpt_email_cpf_cnpj.set email
  $store_page.login.inpt_password.set senha
  $store_page.login.inpt_entrar.click
end

Então("devo ser autenticado no site") do
  expect($store_page.login.ul_name_logado.first.text).to eql(DATA["user"]["boas-vindas"])
end

Então("não devo ser autenticado no site") do
  expect($store_page.login.ul_name_logado.first.text).not_to eql(DATA["user"]["boas-vindas"])
  expect($store_page.login.span_user_invalido.text).to have_content(DATA["user"]["invalido"])
end

Então("deve apresentar mensagem de obrigatoriedade para {string}") do |fluxo|
  if fluxo.eql?('email')
    expect($store_page.login.has_span_dados_obrigatorio?(wait: 10)).to be_truthy
    expect($store_page.login.span_dados_obrigatorio.first.text).to have_content(DATA["user"]["obrigatoriedade"])
  elsif fluxo.eql?('senha')
    expect($store_page.login.has_span_dados_obrigatorio?(wait: 10)).to be_truthy
    expect($store_page.login.span_dados_obrigatorio.last.text).to have_content(DATA["user"]["obrigatoriedade"])
  else
    expect($store_page.login.has_span_dados_obrigatorio?(wait: 10)).to be_truthy
    expect($store_page.login.span_dados_obrigatorio.first.text).to have_content(DATA["user"]["obrigatoriedade"])
    expect($store_page.login.span_dados_obrigatorio.last.text).to have_content(DATA["user"]["obrigatoriedade"])
  end
end