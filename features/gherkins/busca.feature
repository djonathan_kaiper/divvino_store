#language: pt

@store
Funcionalidade: Busca de produto no site
  Eu como usuário do sistema
  Quero poder buscar um produto no site
  Para validar que produto pesquisado foi apresentado

    @busca_produto
    Cenario: Validar busca de produto no site
      Dado acessar o site da divvino
      Quando realizar busca do produto "Cabernet"
      Então deve ser encontrado o produto "Cabernet"