class Login < SitePrism::Page
  element :a_entrar_login, '#loginAutoUser'
  element :inpt_email_cpf_cnpj, '#user'
  element :inpt_password, '.passUser'
  element :inpt_entrar, '#loginSubmit'
  elements :ul_name_logado, '.top_menu_ul a'
  element :span_user_invalido, '#errorLogin'
  elements :span_dados_obrigatorio, '.error_msg'
end