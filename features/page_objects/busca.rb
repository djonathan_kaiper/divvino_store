class Produto < SitePrism::Section
  element :div_adicionar ,'.container_btn_add'
  element :span_quantidade_produto, '.select2-selection__rendered'
  element :a_name_produto, 'div.prod_title_beverages a'
  element :div_valor_real, '.prod_price_int_sale_price'
  element :div_valor_centavo, '.prod_price_cen_sale_price'

  def compravel?
    has_div_adicionar?
  end

  def get_link
    a_name_produto[:href]
  end
end

class Busca < SitePrism::Page
  element :inpt_search, '#search_box_desktop'
  element :btn_lupa_search, '.icon-lupa'
  sections :card_produto, Produto, '.product_grid'

  def valida_compravel
    card_produto.index { |produto| produto.compravel? }.to_i
  end
end