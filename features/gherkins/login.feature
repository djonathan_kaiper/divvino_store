#language: pt

@store
Funcionalidade: Login no site
  Eu como usuário do sistema
  Quero poder realizar login com o meu email e senha
  Para validar que a atenticação foi realizada

    @login_sucesso
    Cenario: Validar login com sucesso no site
      Dado acessar o site da divvino
      Quando realizar login com "test@automation.com.br" e "123456"
      Então devo ser autenticado no site

    @login_dados_invalido
    Esquema do Cenario: Validar login com dados inválido no site
      Dado acessar o site da divvino
      Quando realizar login com <email> e <senha>
      Então não devo ser autenticado no site

        Exemplos:
          | email                    | senha    |
          | "test@automation.com.br" | "654321" |
          | "test@testes.com.br"     | "123456" |
    
    @obrigatoriedade_login
    Esquema do Cenario: Validar obrigatoriedade dos campo de login no site
      Dado acessar o site da divvino
      Quando realizar login com <email> e <senha>
      Então deve apresentar mensagem de obrigatoriedade para <fluxo>

        Exemplos:
          | fluxo   | email                    | senha    |
          | "email" | ""                       | "123456" |
          | "senha" | "test@automation.com.br" | ""       |
          | "ambos" | ""                       | ""       |