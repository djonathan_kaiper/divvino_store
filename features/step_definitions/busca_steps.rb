Quando("realizar busca do produto {string}") do |produto|
  $store_page.busca.inpt_search.set produto
  $store_page.busca.btn_lupa_search.click
end

Então("deve ser encontrado o produto {string}") do |produto|
  @produto = $store_page.busca.card_produto[$store_page.busca.valida_compravel]
  @produto_name_busca = @produto.a_name_produto.text
  @produto_quantidade_busca = @produto.span_quantidade_produto.text
  @produto_preco_busca =  "R$ #{ @produto.div_valor_real.text + @produto.div_valor_centavo.text }"
  @produto_link = @produto.get_link
  expect(@produto_name_busca).to have_content(produto)
end